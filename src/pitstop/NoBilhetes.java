package pitstop;

/**
 *
 * @author Jefferson
 */
class NoBilhetes {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/

    private Bilhetes objeto;
    private NoBilhetes referencia;


    
    // _c = código
    public NoBilhetes(int _c, int _b) {
        
        Bilhetes u = new Bilhetes(_c, _b);
        
        this.setObjeto(u);
        
    }
    public NoBilhetes(Bilhetes _b) {
        
        Bilhetes u = _b;
        
        this.setObjeto(u);
        
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/

    // Método set para registrar a referência do próximo nó da pilha.
    // _rPN = referência do próximo nó
    public void setProximo(NoBilhetes _rPN) {
        this.referencia = _rPN;
    } 

    // Método get para resgatar a referência do próximo nó da pilha.
    public NoBilhetes getProximo() {
        return this.referencia;
    }

    // Método set para registrar a instância do usuário.
    // _u = usuário
    public void setObjeto(Bilhetes _u){
        this.objeto = _u;
    }
    
    // Método get para resgatar o objeto Usuaário.
    public Bilhetes getObjeto() {
        return this.objeto;
    }
    
}
