package pitstop;

class Pessoa {
    
    int codPessoa;
    String nomePessoa;
    String cellPessoa;
    String emailPessoa;
    int numBilhete;
    int codBilhete;

    public Pessoa(int codPessoa) {
        this.codPessoa = codPessoa;
    }

    public int getCodPessoa(){
        return this.codPessoa;
    }
    public void setCodPessoa(int _c){
        this.codPessoa = _c;
    }
    public String getNomePessoa() {
        return this.nomePessoa;
    }

    public String getCellPessoa() {
        return this.cellPessoa;
    }

    public String getEmailPessoa() {
        return this.emailPessoa;
    }

    public void setCellPessoa(String _s) {
        this.cellPessoa = _s;
    }

    public void setEmailPessoa(String _s) {
        this.emailPessoa = _s;
    }

    public int getCodBilhete(){
        return this.codBilhete;
    }
    public int getNumBilhete(){
        return this.numBilhete;
    }

    public void setCodBilhete(int _i){
        this.codBilhete = _i;
    }
    public void setNumBilhete(int _i){
        this.numBilhete = _i;
    }
    public void setNomePessoa(String _s) {
        this.nomePessoa = _s;
    }
           
}
