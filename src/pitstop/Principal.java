package pitstop;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

import java.util.Scanner;

public class Principal{
    public static void main(String[] args) throws IOException{
        Fila fila = new Fila();
        Pilha pilha = Pilha.getIngressos();
        Lista lista = new Lista();
        lista.inicializarLE();
        
        File log = new File("Log.txt");
        FileWriter fw = new FileWriter(log, true);
        PrintWriter pw = new PrintWriter(fw);

        try{
            pw.println("==========Inicio===============");
            fila.queue();
            for(int i = 1; i <= 80; i++){
                fila.enqueue(i);
            }
            pw.println("\tEnchendo a fila de espera com os inscritos");
            int resposta;
            do{
                Scanner input = new Scanner(System.in);
                System.out.println(" ===================================");
                System.out.println("|  1 - Entrar na fila de espera     |");
                System.out.println("|  2 - Pegar entrada                |");
                System.out.println("|  3 - Sair da sala                 |");
                System.out.println("|  4 - Mostrar dados da Fila        |");
                System.out.println("|  5 - Mostrar dados da Pilha       |");
                System.out.println("|  6 - Mostrar dados da Lista       |");
                System.out.println("|  0 - Sair da aplicação            |");
                System.out.println(" ===================================\n");

                resposta = input.nextInt();

                switch(resposta){
                    case 0:
                        lista.limparLE();
                        System.out.println("Saindo da aplicação...");                        
                        pw.println("Saindo da aplicação...");
                    break;

                    case 1:                   
                        pw.println("\t\t\t\tEntrando na fila");
                        pw.println("mais um desesperado na fila");
                        fila.enqueue(fila.finalDaFila.getObjeto().getCodPessoa()+1);
                        System.out.println("Uma nova pessoa acabou de entrar na fila!");                        
                        pw.println("*");
                    break;

                    case 2:                    
                        pw.println("\t\t\t\tPegando Bilhete");
                        pw.println("Pegando entrada na bilheteria...");
                        if(pilha.isEmpty() || fila.size()==0){
                            System.out.println("Fila ou pilha estão vazias!");
                            pw.println("Ops! Fila ou pilha estão vazias!");                        
                            pw.println("*");
                            break;
                        }
                        String nome, eMail, cell;
                        input.nextLine();
                        do{
                            System.out.println("Informe seu nome: ");
                            nome = input.nextLine();
                            System.out.println("Informe seu eMail: ");
                            eMail = input.nextLine();
                            System.out.println("Informe o numero do celular: ");
                            cell = input.nextLine();
                        }while(nome.equals("") || eMail.equals(""));
                        Pessoa agraciado = fila.dequeue().getObjeto();
                        Bilhetes entrada =  pilha.pop();
                        agraciado.setNomePessoa(nome);
                        agraciado.setEmailPessoa(eMail);
                        agraciado.setCellPessoa(cell);
                        agraciado.setCodBilhete(entrada.getCodigoBilhete());
                        agraciado.setNumBilhete(entrada.getNunEntrada());                        
                        lista.inserirDados(agraciado, lista.getQuantidadeDeNos());
                        pw.println(nome+" pegou o bilhete com o Cod "+entrada.getCodigoBilhete());                      
                        pw.println("*");
                        
                    break;
                    case 3:                  
                        pw.println("\t\t\t\tSair Sala");
                        pw.println("Saindo da Sala...");

                        System.out.println("Digite o codigo da pessoa que vai sair da sala: ");
                        int saindo = input.nextInt();
                        pw.println("cod digitado "+ saindo);

                        Pessoa pessoa = lista.removerDados(saindo-1);
                        if(pessoa !=null){
                            pilha.push(pessoa.getCodBilhete(), pessoa.getNumBilhete());
                            System.out.println(pessoa.getNomePessoa()+" acabou de sair da sala");
                            pw.println(pessoa.getNomePessoa()+" acabou de sair da sala");
                            pw.println("*");
                            break;
                        }
                        System.out.println("Codigo invalido");
                        pw.println("Codigo invalido");
                        pw.println("*");
                        break;
                    case 4:
                        pw.println("\t\t\t\tMostrando Fila Completa");
                        System.out.println(fila.print());
                    break;
                    case 5:
                        pw.println("\t\t\t\tMostrando Pilha Completa");
                        System.out.println(pilha.print());
                    break; 
                    case 6:
                        pw.println("\t\t\t\tMostrando Lista Completa");
                        System.out.println(lista.imprimirLista());
                    break;
                    default:
                        System.out.println("Opção invalida");
                        pw.println("Opção invalida");
                    break;
                }

            }
            while(resposta != 0);
            pw.println("==========Fim===============/n");
            pw.close();
            fw.close();
        }catch (Exception e) {
            System.out.println(e);
        }
    }
}