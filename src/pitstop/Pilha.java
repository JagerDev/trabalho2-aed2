package pitstop;

import java.util.Random;

public class Pilha {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/
    
    private
        NoBilhetes topo;
        int quantidadeDeNos;
        int full;
 
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
        
    public Pilha(){ }
    
    /*-----------------------------------
         MÉTODOS get E set DA CLASSE
    -----------------------------------*/
    
    // Método para acessar o topo da pilha.
    public NoBilhetes getTopo() {
        return this.topo;
    }

    // Método para alterar (registrar) o topo da pilha.
    public void setTopo(final NoBilhetes _topo) {
        this.topo = _topo;
    }

    // Método para acessar a quantidade de nós da pilha.
    public int getQuantidadeDeNos() {
        return this.quantidadeDeNos;
    }

    // Método para atualizar a quantidade de nós da pilha.
    // _qDN = quantidade de nós
    public void setQuantidadeDeNos(final int _qDN) {
        this.quantidadeDeNos = _qDN;
    }

    /*-----------------------------------
           OPERAÇÕES DO TAD PILHA E
    -----------------------------------*/

    // Método para criar a pilha encadeada.
    public void criarPilha() {
        this.setTopo(null);
        this.setQuantidadeDeNos(0);
    }

    // Método para verificar se a pilha está vazia.
    public boolean isEmpty() {
        return this.getTopo() == null;
    }

    // Método para empilhar dados na pilha.
    public void push(Bilhetes _b) {

        final NoBilhetes novoNo = new NoBilhetes(_b);

        // Se a pilha não estiver vazia,
        // novoNo vai apontar para o nó que está no topo da pilha,
        // Ou seja, guarda o endereço do nó que está no topo da pilha.
        if (!this.isEmpty())
            novoNo.setProximo(this.getTopo());
        else
            novoNo.setProximo(null);

        // Atualiza o topo da pilha:
        // - novoNo agora é o nó que está no topo da pilha.
        this.setTopo(novoNo);

        // Atualiza a quantidade de nós da pilha.
        this.setQuantidadeDeNos(this.getQuantidadeDeNos() + 1);

    }
    public void push(int _s, int _e) {

        final NoBilhetes novoNo = new NoBilhetes(_s, _e);

        // Se a pilha não estiver vazia,
        // novoNo vai apontar para o nó que está no topo da pilha,
        // Ou seja, guarda o endereço do nó que está no topo da pilha.
        if (!this.isEmpty())
            novoNo.setProximo(this.getTopo());
        else
            novoNo.setProximo(null);

        // Atualiza o topo da pilha:
        // - novoNo agora é o nó que está no topo da pilha.
        this.setTopo(novoNo);

        // Atualiza a quantidade de nós da pilha.
        this.setQuantidadeDeNos(this.getQuantidadeDeNos() + 1);

    }

    // Método para desempilhar dados da pilha.
    public Bilhetes pop() {

        // Se foi possível apresentar os dados do nó
        // Que está no topo, efetiva a remoção.
        if (this.top()) {

            // Recebe o endereço de quem está no topo da pilha.
            final NoBilhetes auxiliar = this.getTopo();

            // O nó que ficará no topo da pilha é o que vem
            // depois do 1º nó (o que estava no topo).
            this.setTopo(auxiliar.getProximo());

            // Desconecta o nó (controlado por 'auxiliar') da pilha.
            auxiliar.setProximo(null);

            // Atualiza a quantidade de nós da pilha.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos() - 1);
            return auxiliar.getObjeto();

        }

        return null;

    }

    // Método para apresentar os dados de quem está no topo da pilha.
    public boolean top() {

        // Se a pilha não estiver vazia,
        // apresenta os dados do nó que está no topo da pilha.
        if (!this.isEmpty()) {

            System.out.println("Sala: "+this.getTopo().getObjeto().getCodigoBilhete()+"\nEntrada: "+this.getTopo().getObjeto().getNunEntrada());
           

            return true;

        }

        return false;

    }

    // Mostrando a quantidade de nós da pilha.
    public int size() {
        return this.getQuantidadeDeNos();
    }

    // Esvaziando a pilha encadeada.
    public boolean clear() {

        this.setTopo(null);
        this.setQuantidadeDeNos(0);

        return this.getTopo() == null;
    }

    // Mostrando os dados da pilha encadeada.
    public String print() {

        // Se a pilha não estiver vazia,
        if (!this.isEmpty()) {

            // Cria uma cópia do endereço de quem está no topo
            // e coloca no atributo 'auxiliar'.
            NoBilhetes auxiliar = this.getTopo();

            // Enquanto o endereço de 'auxiliar'
            // é diferente de null, percorre a pilha.
            String msg = "";
            while (auxiliar != null) {

                msg+=("\nSala: " +auxiliar.getObjeto().getCodigoBilhete());
                msg+=(" - Entrada: "+auxiliar.getObjeto().getNunEntrada());

                // Avança para o próximo nó da pilha.
                auxiliar = auxiliar.getProximo();
            }
            return msg+"\n-------------\n";
        }
        return "Pilha fazia";

    }
    private static Bilhetes[] GerardorBilhetes(){
        Bilhetes[] bilhetesGerados = new Bilhetes[75];

        for(int c = 0; c < 75; c++){
            bilhetesGerados[c] = new Bilhetes(((c/15) + 1), (c+1));
        }

        return bilhetesGerados;
    }
    public static Pilha getIngressos(){
        Bilhetes[] bilhetesGerados = GerardorBilhetes();
        int[] codRetirados = new int[75];

        Pilha bilhetes = new Pilha();
        Random random = new Random();
        int contRetidadas = 0;

        while (contRetidadas < 74) {
            int randomCod = random.nextInt(bilhetesGerados.length);

            if (!contains(randomCod, codRetirados)) {
                codRetirados[randomCod] = randomCod;
                bilhetes.push(bilhetesGerados[randomCod]);

                contRetidadas++;
            }

        }

        return bilhetes;
    }
    private static boolean contains(int cod, int[] arrCod) {
        for (int i = 0; i < arrCod.length; i++) {
            if (cod == arrCod[i]) {
                return true;
            }
        }

        return false;
    }



}