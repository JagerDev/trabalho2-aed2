package pitstop;

class Bilhetes {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private int codigoBilhete;
    private int codBilhete;
    
    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // _codB  = código do bilhete
    // _numB = numero do bilhete
    public Bilhetes(int _codB, int _numB) {
        this.codigoBilhete = _codB;
        this.codBilhete   = _numB;
    }
    public Bilhetes() {}
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/
    
    // Método para resgatar o código da bilhete
    public int getCodigoBilhete() {
        return this.codigoBilhete;
    }

    // Método para registrar o código da bilhete
    // _codB  = código da bilhete
    public void setCodigoBilhete(int _codB) {
        this.codigoBilhete = _codB;
    }

    // Método para resgatar o numero da entrada
    public int getNunEntrada() {
        return this.codBilhete;
    }

    // Método para registrar o numero da entrada
    // _numB = numero da entrada
    public void setNunEntrada(int _numB) {
        this.codBilhete = _numB;
    }
           
}
