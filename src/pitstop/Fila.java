package pitstop;

public class Fila {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/
    
    private
        NoPessoa inicioDaFila;
        int quantidadeDeNos;
        NoPessoa finalDaFila;
              
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
        
    public Fila(){ }
    
    /*-----------------------------------
         MÉTODOS get E set DA CLASSE
    -----------------------------------*/

    // Método para acessar o início da fila.
    public NoPessoa getInicioDaFila() {
        return this.inicioDaFila;
    }

    // Método para alterar o início da fila.
    public void setInicioDaFila(NoPessoa _inicioDaFila) {
        this.inicioDaFila = _inicioDaFila;
    }

    // Método para resgatar a quantidade de nós.
    public int getQuantidadeDeNos() {
        return this.quantidadeDeNos;
    }

    // Método para alterar a quantidade de nós.
    public void setQuantidadeDeNos(int _quantidadeDeNos) {
        this.quantidadeDeNos = _quantidadeDeNos;
    }
    
    // Método para acessar o final da fila.
    public NoPessoa getFinalDaFila() {
        return this.finalDaFila;
    }

    // Método para alterar o final da fila.
    public void setFinalDaFila(NoPessoa _finalDaFila) {
        this.finalDaFila = _finalDaFila;
    }

    /*-----------------------------------
           OPERAÇÕES DO TAD FILA E
    -----------------------------------*/
    
    // Cria a fila encadeada.
    public void queue(){
        
        this.setInicioDaFila(null);
        this.setQuantidadeDeNos(0);
        this.setFinalDaFila(null);
   
    }
    
    // Método que verifica se a fila está vazia.
    private boolean isEmpty() {
        
        // Quando o início e final da fila apontam para null,
        // conclui-se que a Fila está vazia.
        return ( (this.getInicioDaFila() == null) && (this.getFinalDaFila() == null) );
  
    }
    
    // Método para enfileirar dados.
    public void enqueue(int _c){
        
        // Cria o nó e registra os dados do usuário.
        NoPessoa novoNo = new NoPessoa(_c);
        
        // O campo 'próximo' do novoNo vai registrar null.
        novoNo.setProximo(null);
        
        // Se a fila estiver vazia,
        // novoNo fica sendo o início e o final da fila.
        if ( this.isEmpty() ){
            
            this.setInicioDaFila(novoNo);
            this.setFinalDaFila(novoNo);
        
        }else{
            
            // Caso contrário, adiciona o nó após o último nó da fila.
            this.getFinalDaFila().setProximo(novoNo);
            
            // E diz que novoNo é o novo final da fila.
            this.setFinalDaFila(novoNo);
        
        }
        
        // Atualiza a quantidade de nós da fila.
        this.setQuantidadeDeNos(this.getQuantidadeDeNos()+1);

    }
    
    // Método para desenfileirar dados.
    // O nó a ser removido sempre será o que está na 1ª posição da Fila.
    public NoPessoa dequeue(){
        
        // Se conseguiu apresentar os dados do usuário,
        // avança o inicio da fila para o próximo nó.
        if ( this.front() ){

            // Recebe o endereço de quem está no início da fila.
            NoPessoa auxiliar = this.getInicioDaFila();

            // O início da fila passa a ser o próximo nó, ou seja,
            // o que vem depois do 1º nó.
            this.setInicioDaFila(auxiliar.getProximo());

            // Caso 1: fila com apenas 1 elemento
            if ( this.getQuantidadeDeNos() != 1 ){
            
                // Desconecta o nó (controlado por 'auxiliar') da fila.
                auxiliar.setProximo(null);

            }else{

                // Caso específico: fila com apenas 1 elemento.
                this.setFinalDaFila(null);
                
            }

            // Atualiza a quantidade de nós da fila.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()-1);

            return auxiliar;

        }
        
        return null;
    }
    
    // Método que apresenta os dados do objeto que está na 1ª posição da Fila.
    public boolean front(){
        
        // Método que apresenta os dados do objeto que está na 1ª posição da Fila.
        if ( !this.isEmpty() ){
           System.out.println("Ordem de chagada: "+this.getInicioDaFila().getObjeto().getCodPessoa());
            return true;            
        }        
        return false;
    }

    // Método que retorna todos os dados da Fila em formato de "String".
    public String print(){
        
        String informacoes = "";

        // Se a fila não estiver vazia, percorre a fila. 
        if ( !this.isEmpty() ){
           
            // Cria uma cópia do endereço de quem está
            // no início da fila e coloca no atributo 'auxiliar'.
            NoPessoa auxiliar = this.getInicioDaFila();
            
            // Enquanto o endereço do nó for diferente de null.
            while ( auxiliar != null ){
                
                // Concatena as informações do usuário atual
                //  com as informações anteriores.
                informacoes += auxiliar.getObjeto().getCodPessoa()+"\n";

                // Avança para o próximo nó da fila.
                auxiliar = auxiliar.getProximo();

            }
            
        }
        
        return informacoes;
        
    }

    // Método privado -> Pesquisa sequencial.
    // _c  = código que talvez exista na fila.
    private NoPessoa pesquisaSequencial(int _c){

        // Atributo inicia com o endereço do nó que está no início da fila.
        NoPessoa endereco = this.getInicioDaFila();
            
        // Localizando o endereço do nó que será alterado, caso exista.
        // Só sai desta estrutura de repetição caso encontre o código ou passe do final da fila.
        while (  (_c != endereco.getObjeto().getCodPessoa()) && (endereco.getProximo() != null) )
            endereco = endereco.getProximo();

        //
        if ( (_c == endereco.getObjeto().getCodPessoa()) && (endereco != null) )
            return endereco;
        else
            return null;
        
    }

    // Método que altera os dados de um determinado nó da fila.
    // A chave de pesquisa será o código de usuário.
    // _c  = código que talvez exista na fila
    // _nc = novo código
    // _nn = novo nome
    public boolean toAccess(int _c, int _nc){
            
        // Se a fila não estiver vazia,
        if (!this.isEmpty()){
            
            // Chama o método de pesquisa e informa o endereço do nó
            // caso a chave seja encontrada ou então null se não encontrada.
            NoPessoa endereco = pesquisaSequencial(_c);
            
            // Verificando se realmente encontrou o usuário para substituição.
            if ( endereco != null ){
                
                // Altera os dados do nó que está no endereço 'endereco'.
                endereco.getObjeto().setCodPessoa(_nc);
                return true;

            }
            
        }
        
        return false;
        
    }
    
    // Método que retorna a quantidade de pessoas presentes na fila.
    public int size(){
        
        return this.getQuantidadeDeNos();
 
    }
    
    // Método para limpar / esvaziar a fila.
    public void clear(){
        
        if ( !this.isEmpty() ){
            
            this.setInicioDaFila(null);
            this.setQuantidadeDeNos(0);
            this.setFinalDaFila(null);
        
        }
        
    }    
}
