package pitstop;


class NoPessoa {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/

    private Pessoa objeto;
    private NoPessoa referencia;

    /*-----------------------------------
             CONSTRUTOR DA CLASSE
      -----------------------------------*/
    
    // _s = sala
    // _e = entrada
    public NoPessoa(int _e) {
        
        Pessoa u = new Pessoa(_e);
        
        this.setObjeto(u);
        
    }
    public NoPessoa(Pessoa _p) {
        
        Pessoa u = _p;
        
        this.setObjeto(u);
        
    }
    
    /*-----------------------------------
               MÉTODOS DA CLASSE
      -----------------------------------*/

    // Método set para registrar a referência do próximo nó da pilha.
    // _rPN = referência do próximo nó
    public void setProximo(NoPessoa _rPN) {
        this.referencia = _rPN;
    } 

    // Método get para resgatar a referência do próximo nó da pilha.
    public NoPessoa getProximo() {
        return this.referencia;
    }

    // Método set para registrar a instância do usuário.
    // _u = usuário
    public void setObjeto(Pessoa _u){
        this.objeto = _u;
    }
    
    // Método get para resgatar o objeto Usuaário.
    public Pessoa getObjeto() {
        return this.objeto;
    }
    
}
